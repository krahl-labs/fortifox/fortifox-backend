import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn, } from 'typeorm';
import * as bcrypt from 'bcrypt'

@Entity()
export class User {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({default: true})
    isActive: boolean;

    @BeforeInsert()
    @BeforeUpdate()
    async hashPassword() {
        if(this.password){
            this.password = await bcrypt.hash(this.password, 10);
        }
    }
}