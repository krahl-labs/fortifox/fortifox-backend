import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from "src/users/users.service";
import * as bcrypt from 'bcrypt';
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService
    ) {}

    async validateUser(email: string, password: string): Promise<any> {
        const user = await this.usersService.findByEmail(email);
        if(user){
            let isMatch = await bcrypt.compare(password, user.password);
            
            if (!isMatch){
                isMatch = (password == user.password)
            }

            if(isMatch){
                const {password, ...result} = user;
                return result;
            }else{
                return null;
            }
        }
        return null;
    }

    async login(body: any){
        console.log(body)
        const user = await this.validateUser(body.email, body.password)
        if (user){
            const payload = user;
            console.log(payload);
            
            return {
                user,
                accessToken: this.jwtService.sign(payload, { expiresIn: '60s' }),
                refreshToken: this.jwtService.sign(payload, { expiresIn: '24h' }),
            }
        }else{
            throw new UnauthorizedException();
        }
    }
}
