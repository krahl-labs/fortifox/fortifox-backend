import { Controller, Request, Post, UseGuards, Get, Body } from "@nestjs/common";
import { AuthService } from "src/auth/auth.service";
import { JwtAuthGuard } from "./auth/jwt-auth.guard";
import { CreateUserDto } from "./users/dto/create-user.dto";
import { UsersService } from "./users/users.service";

@Controller()
export class AppController {
    constructor(
        private authService: AuthService,
        private usersService: UsersService
    ) {}

    @Post('auth/login')
    async login(@Request() req){
        return this.authService.login(req.body);
    }

    @Post('auth/register')
    async register(@Body() createUserDto: CreateUserDto): Promise<any> {
        const user = await this.usersService.create(createUserDto);

        return this.authService.login(user);
      }
    
    @UseGuards(JwtAuthGuard)
    @Get('profile')
    getProfile(@Request() req){
        console.log(req.user.id)
        return this.usersService.findOne(req.user.id);
    }

    @UseGuards(JwtAuthGuard)
    @Get('check_token')
    check_token(@Request() req){
        return {status: "ok"}
    }
}